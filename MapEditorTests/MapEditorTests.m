//
//  MapEditorTests.m
//  MapEditorTests
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MEDJsonGenerator.h"

#import "MEDBeaconPoint.h"
#import "MEDFurniture.h"

@interface MapEditorTests : XCTestCase

@end

@implementation MapEditorTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)aExample
{
    CGRect rect1 = CGRectMake(20.0, 40.0, 234.3, 23.3);
    
    NSString *size, *position;
    UInt16 minor, major;
    
    minor = 10;
    major = 20;
    size = NSStringFromCGSize(rect1.size);
    position = NSStringFromCGPoint(rect1.origin);
    BOOL flipH = YES;
    NSDictionary *dict1 = @{@"position" : position,
                            @"size": size,
                            @"minor" : @(minor),
                            @"major" : @(major),
                            @"flipH" : @(flipH)};
    
    NSArray *furnitures = @[dict1];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:furnitures options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", jsonString);
    
    NSArray *parsedArray = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"parse array \n %@", parsedArray);
    for (NSDictionary *furnitureDict in parsedArray) {
        NSString *posString = furnitureDict[@"position"];
        CGPoint extractedPoint;
        extractedPoint = CGPointFromString(posString);
        NSLog(@"x: %f y: %f", extractedPoint.x, extractedPoint.y);
        
    }
    
}

- (void)testGenerateFurnitures
{
    MEDFurniture *f1, *f2, *f3;
    
    f1 = [[MEDFurniture alloc] init];
    f1.furnitureId = 23;
    f1.stencilId = 1;
    f1.flippedHorizontal = NO;
    f1.flippedVertical = NO;
    f1.rect = CGRectMake(23.0, 234.0, 347.4, 863.4);
    f1.pointId = 2;
    
    f2 = [[MEDFurniture alloc] init];
    f2.furnitureId = 24;
    f2.stencilId = 1;
    f2.flippedHorizontal = YES;
    f2.flippedVertical = NO;
    f2.rect = CGRectMake(23.0, 234.0, 347.4, 863.4);
    f2.pointId = 1;
    
    f3 = [[MEDFurniture alloc] init];
    f3.furnitureId = 25;
    f3.stencilId = 2;
    f3.flippedHorizontal = YES;
    f3.flippedVertical = YES;
    f3.rect = CGRectMake(748.0, 382.0, 927.4, 334.9);
    f3.pointId = 3;
    
    NSArray *furnitures = @[f1, f2, f3];
    
    NSString *jsonString;
    
    MEDJsonGenerator *generator;
    generator = [[MEDJsonGenerator alloc] init];
    
    jsonString = [generator jsonFurnituresWithFurnituresArray:furnitures];
    
    NSLog(@"jsonString\n %@", jsonString);
    
    NSArray *outputFurnitures;
    outputFurnitures = [generator furnituresWithJsonString:jsonString];
    
    for (MEDFurniture *furniture in outputFurnitures) {
        NSLog(@"FurId:%d StencilId:%d flipH:%d flipV:%d rect:%@ pointID%d", furniture.furnitureId, furniture.stencilId, furniture.flippedHorizontal,
              furniture.flippedVertical, NSStringFromCGRect(furniture.rect), furniture.pointId);
    }
}

- (void)testGeneratePoints
{
    MEDBeaconPoint *point1, *point2, *point3;
    
    point1 = [[MEDBeaconPoint alloc] init];
    point1.pointId = 1;
    point1.position = CGPointMake(23.5, 76.2);
    point1.major = 23;
    point1.minor = 46;
    point1.uuid = @"23SD-32ASD3SAD-DSA3-JK8";
    
    point2 = [[MEDBeaconPoint alloc] init];
    point2.pointId = 2;
    point2.position = CGPointMake(74.3, 893.6);
    
    point3 = [[MEDBeaconPoint alloc] init];
    point3.pointId = 3;
    point3.position = CGPointMake(63.8, 935.6);
    point3.major = 22;
    point3.minor = 11;
    point3.uuid = @"23SD-32ASD3SAD-DSA3-JK8";
    
    
    [point1 addToGraphWithPoint:point3];
    [point1 addToGraphWithPoint:point2];
    
    //    [point2 addToGraphWithPoint:point3];
    
    NSArray *points = @[point1, point2, point3];
    
    NSString *jsonString;
    
    MEDJsonGenerator *generator;
    generator = [[MEDJsonGenerator alloc] init];
    
    jsonString = [generator jsonPointsWithPointsArray:points];
    
    NSLog(@"jsonString\n %@", jsonString);
    
    
    NSArray *outputPoints;
    outputPoints = [generator pointsWithJsonString:jsonString];
    
    for (MEDBeaconPoint *point in outputPoints) {
        NSLog(@"PointId:%d major:%d minor:%d uuid:%@ position:%@ graph:%@", point.pointId, point.major, point.minor, point.uuid, NSStringFromCGPoint(point.position), point.graphDict);
    }
    
    
//    for (MEDBeaconPoint *point in points) {
//        NSLog(@"graph: %@", point.graphDict);
//    }
}



@end
