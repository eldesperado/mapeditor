//
//  ShapeMenuPopoverViewController.h
//  MapEditor
//
//  Created by El Desperado on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShapeMenuPopoverViewControllerDelegate;

@interface ShapeMenuPopoverViewController
    : UIViewController<UITextFieldDelegate> {
}

@property(nonatomic, weak)
    id<ShapeMenuPopoverViewControllerDelegate> shapePopoverDelegate;
@property(strong, nonatomic) IBOutlet UITextField* widthTextField;
@property(strong, nonatomic) IBOutlet UITextField* heightTextField;
@property(strong, nonatomic) IBOutlet UITextField* xTextField;
@property(strong, nonatomic) IBOutlet UITextField* yTextField;
@property(strong, nonatomic) IBOutlet UITextField* nameTextField;
- (IBAction)doneAction:(id)sender;
- (IBAction)flipHorizontalAction:(id)sender;
- (IBAction)flipVerticalAction:(id)sender;
- (IBAction)deleteAction:(id)sender;

@end

@protocol ShapeMenuPopoverViewControllerDelegate<NSObject>
@required
- (void)shapeDataChangedWithWidth:(CGFloat)width
                           height:(CGFloat)height
                                x:(CGFloat)x
                                y:(CGFloat)y;
- (void)shapeDataChangedWithRect:(CGRect)rect
                            name:(NSString *)name;
- (void)flipShapeHorizontal;
- (void)flipShapeVertical;
- (void)deleteShape;

@end