//
//  ADLineView.h
//  MapEditor
//
//  Created by Dat Truong on 4/29/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADLineView : UIView

@property (nonatomic) BOOL isSelected;
@property CGPoint startPoint;
@property CGPoint endPoint;
- (instancetype)initWithPoint:(CGPoint) a to:(CGPoint) b;

@end
