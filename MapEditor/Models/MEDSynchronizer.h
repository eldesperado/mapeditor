//
//  MEDSynchronizer.h
//  MapEditor
//
//  Created by El Desperado on 7/23/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEDSynchronizer : NSObject<NSURLSessionDelegate>

@property(nonatomic, copy) NSURL* baseUrl;
@property(nonatomic, copy) NSURLSession* client;

+ (MEDSynchronizer*)sharedService;
- (void)uploadMapDataWithMapId:(NSString*)mapId
                         image:(UIImage*)image
                    completion:(void (^)(NSDictionary *data, NSError *error))completion;
- (void)addOrUpdateMapDataWithMapId:(NSString*)mapId
                            mapData:(NSString*)mapData
                              alias:(NSString*)alias
                              level:(NSUInteger)level
                         completion:(void (^)(NSDictionary *data, NSError *error))completion;
- (void)downloadMapDataThenCompletion:(void (^)(NSDictionary *data, NSError *error))completion;

@end
