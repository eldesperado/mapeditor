//
//  UIViewController+OrientationFix.h
//  MapEditor
//
//  Created by Dat Truong on 2014-07-24.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (OrientationFix)

@end
