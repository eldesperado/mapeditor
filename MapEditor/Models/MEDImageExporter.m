//
//  MEDImageExporter.m
//  MapEditor
//
//  Created by Dat Truong on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "MEDImageExporter.h"

@implementation MEDImageExporter

- (UIImage *)imageFromView:(UIView *)sourceView
{
    UIImage *image;
    
    UIGraphicsBeginImageContextWithOptions(sourceView.bounds.size, sourceView.opaque, 0.0);
    [sourceView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    image = img;
    
    return image;
}

- (void)exportImageFromView:(UIView *)sourceView withName:(NSString *)imageName
{
    UIImage *exportedImage;
    UIView *exportingView;
    exportingView = [[UIView alloc] initWithFrame:sourceView.frame];
    exportingView.backgroundColor = [UIColor whiteColor];
    UIView * copyOfView =
    [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:sourceView]];
    [exportingView addSubview:copyOfView];
    exportedImage = [self imageFromView:exportingView];
    
    // Create path.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", imageName]];
    
    // Save image.
    [UIImageJPEGRepresentation(exportedImage, 1.0) writeToFile:filePath atomically:YES];
}

@end
