//
//  ADLineView.m
//  MapEditor
//
//  Created by Dat Truong on 4/29/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ADLineView.h"
#import "MEDBeaconPoint.h"

@implementation ADLineView

//- (instancetype)init
//{
//    self = [self initWithFrame:CGRectMake(0, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)];
//    if (self) {
//        
//    }
//    return self;
//}


- (instancetype)initWithPoint:(CGPoint) a to:(CGPoint) b
{
    if (a.x == b.x && a.y == b.y) {
        return nil;
    }
    
    CGRect lineRect;
    float rectWidth = [MEDBeaconPoint lengthFrom:a to:b];
    lineRect = CGRectMake(a.x + POINT_SIZE / 2,
                          a.y + POINT_SIZE / 2,
                          rectWidth - POINT_SIZE / 2, 1.0);
    
    ADLineView *lineView = [[ADLineView alloc] initWithFrame:lineRect];
    
    lineView.startPoint = a;
    lineView.endPoint = b;
    
    CGFloat angle = atan2(a.y - b.y, a.x - b.x);
    lineView.layer.anchorPoint = CGPointMake(0.0, 0.0);
    lineView.center = CGPointMake(b.x, b.y);
    lineView.transform = CGAffineTransformMakeRotation(angle);
    
    return lineView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self setNeedsDisplay];
}

- (void) drawRect:(CGRect)rect
{
    UIColor *backgroundColor;
    backgroundColor = self.isSelected ? [UIColor blueColor] : [UIColor yellowColor];
    
    [backgroundColor setFill];
    UIRectFill(rect);
    
    CAShapeLayer *shapelayer = [CAShapeLayer layer];
    [self.layer addSublayer:shapelayer];
    
    //draw a line
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0, 0.0)];
    [path addLineToPoint:CGPointMake(rect.size.width, 0.0)];

    CGFloat dashPattern[] = {2,6,4,2};
    [path setLineDash:dashPattern count:4 phase:3];
    
    UIColor *fill = [UIColor grayColor];
    shapelayer.strokeStart = 0.0;
    shapelayer.strokeColor = fill.CGColor;
    shapelayer.lineWidth = 2.0;
    shapelayer.lineJoin = kCALineJoinMiter;
    shapelayer.lineDashPattern = @[@(10), @(7)];
    shapelayer.lineDashPhase = 3.0f;
    shapelayer.path = path.CGPath;
}


@end
