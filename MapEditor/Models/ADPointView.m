//
//  ADPointView.m
//  MapEditor
//
//  Created by Dat Truong on 2014-05-01.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ADPointView.h"



@implementation ADPointView

- (instancetype)initWithPoint:(MEDBeaconPoint *) point
{
    CGPoint position = point.position;
    CGRect pointRect;
    pointRect = CGRectMake(position.x - POINT_SIZE / 2 , position.y  - POINT_SIZE / 2, POINT_SIZE, POINT_SIZE);
    self = [self initWithFrame:pointRect];
    if (self) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandle:)];
        [self addGestureRecognizer:tapGesture];
        
        UIPanGestureRecognizer *panGesture  = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandle:)];
        [self addGestureRecognizer:panGesture];
        
        self.point = point;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - UIGestureRegconizer

- (void)selectPoint
{
    self.isSelected = !self.isSelected;
    if ([self.delegate respondsToSelector:@selector(didSetSelected:)]) {
        [self.delegate didSetSelected:self];
    }
}

- (void) tapHandle:(UIGestureRecognizer *) gr
{
//    CGPoint tapPoint = [gr locationInView:self.superview];
//    CGPoint currentLocation = self.point.location;
//    if (tapPoint.x == currentLocation.x || tapPoint.y == currentLocation.y) {
//        NSLog(@"Khong chi het");
//        return;
//    }
    [self selectPoint];
}


- (void) panHandle:(UIGestureRecognizer *) gr
{
    switch (gr.state) {
        case UIGestureRecognizerStatePossible:
            
            break;
        case UIGestureRecognizerStateBegan:
            if ([self.delegate respondsToSelector:@selector(panDidStarted:)]) {
                [self.delegate panDidStarted:self];
            }
            break;
        case UIGestureRecognizerStateChanged:
            self.center = [gr locationInView:self.superview];
            self.point.position = self.center;
            break;
        case UIGestureRecognizerStateEnded:
            if ([self.delegate respondsToSelector:@selector(panDidEnded:)]) {
                [self.delegate panDidEnded:self];
            }
            break;
        case UIGestureRecognizerStateCancelled:
            
            break;
        case UIGestureRecognizerStateFailed:
            
            break;
            
        default:
            break;
    }
}

- (void) setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGRect bounds = self.bounds;
    
    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
    
    // The circle will be the largest that will fit in the view
    float radius = (MIN(bounds.size.width, bounds.size.height) / 2.0) - 1.0;
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    // Add an arc to the path at center, with radius of radius,
    // from 0 to 2*PI radians (a circle)
    [path addArcWithCenter:center
                    radius:radius
                startAngle:0.0
                  endAngle:M_PI * 2.0
                 clockwise:YES];
    UIColor *circleColor;
    circleColor = [UIColor orangeColor];
    [circleColor setFill];

    [path fill];
    
    
    if (self.isSelected) {
        float outsideRadius = (MIN(bounds.size.width, bounds.size.height) / 2.0) - 1.0;
        UIBezierPath *outsidePath = [[UIBezierPath alloc] init];
        
        // Add an arc to the path at center, with radius of radius,
        // from 0 to 2*PI radians (a circle)
        [outsidePath addArcWithCenter:center
                               radius:outsideRadius
                           startAngle:0.0
                             endAngle:M_PI * 2.0
                            clockwise:YES];
        
        [outsidePath stroke];
    }
}

+ (ADPointView *)nearestPointViewFromPoint:(CGPoint)point inPoints:(NSArray *)points
{
    ADPointView *nearestPointView;
    
    float minLength;
    minLength = MAXFLOAT;
    
    for (ADPointView *pointView in points) {
        float length;
        length = [MEDBeaconPoint lengthFrom:point to:pointView.point.position];
        /*NSLog(@"start:%@, end:%d %@, length:%f, smallest:%f",
              NSStringFromCGPoint(point),
              pointView.point.pointId,
              NSStringFromCGPoint(pointView.point.position),
              length,
              minLength
              );*/
        if (length < minLength) {
            nearestPointView = pointView;
            minLength        = length;
        }
    }
    
    NSLog(@"result: %d", nearestPointView.point.pointId);
    return nearestPointView;
}

@end
