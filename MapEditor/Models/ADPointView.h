//
//  ADPointView.h
//  MapEditor
//
//  Created by Dat Truong on 2014-05-01.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEDBeaconPoint.h"

@class ADPointView;
@protocol ADPointViewDelegate <NSObject>

- (void) didSetSelected:(ADPointView *) currentPoint;
- (void)panDidStarted:(ADPointView *)currentPoint;
- (void)panDidEnded:(ADPointView *)currentPoint;

@end

@interface ADPointView : UIView


@property (nonatomic, assign) id <ADPointViewDelegate> delegate;

@property (nonatomic, strong) MEDBeaconPoint *point;
@property (nonatomic) BOOL isSelected;


- (instancetype)initWithPoint:(MEDBeaconPoint *) point;
- (void)selectPoint;

+ (ADPointView *)nearestPointViewFromPoint:(CGPoint)point inPoints:(NSArray *)points;
@end
