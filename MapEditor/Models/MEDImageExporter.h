//
//  MEDImageExporter.h
//  MapEditor
//
//  Created by Dat Truong on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEDImageExporter : NSObject

- (UIImage *)imageFromView:(UIView *)sourceView;
- (void)exportImageFromView:(UIView *)sourceView withName:(NSString *)imageName;

@end
