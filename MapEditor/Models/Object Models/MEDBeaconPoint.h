//
//  MEDBeaconPoint.h
//  MapEditor
//
//  Created by Dat Truong on 2014-07-23.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEDBeaconPoint : NSObject

@property UInt16 pointId;
@property CGPoint position;

@property (nonatomic) UInt16 minor;
@property (nonatomic) UInt16 major;
@property (strong, nonatomic) NSString *uuid;

@property (strong, nonatomic) NSDictionary *graphDict;

// bi-direction
- (void)addToGraphWithPoint:(MEDBeaconPoint *)point;
- (void)removeFromGraphWithPoint:(MEDBeaconPoint *)point;

+ (float) lengthFrom:(CGPoint) a to:(CGPoint) b;
+ (UInt16)idForPointInPointViews:(NSArray *)pointViews;

@end
