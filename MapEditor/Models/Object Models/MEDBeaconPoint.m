//
//  MEDBeaconPoint.m
//  MapEditor
//
//  Created by Dat Truong on 2014-07-23.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "MEDBeaconPoint.h"
#import "ADPointView.h"

@interface MEDBeaconPoint()

@property (strong, nonatomic) NSMutableDictionary *processingGraph;

@end

@implementation MEDBeaconPoint

- (instancetype)init
{
    self = [super init];
    if (self) {
        _processingGraph = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSDictionary *)graphDict
{
    return [_processingGraph copy];
}

- (void)setGraphDict:(NSDictionary *)graphDict
{
    _processingGraph = [NSMutableDictionary dictionaryWithDictionary:graphDict];
}

- (void)setMinor:(UInt16)minor
{
    _minor = minor;
    if (_major != 0) {
        [self setDefaultUUID];
    }
}

- (void)setMajor:(UInt16)major
{
    _major = major;
    if (_minor != 0) {
        [self setDefaultUUID];
    }
}

- (void)setDefaultUUID
{
    _uuid = @"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0";
}

- (void)addToGraphWithPoint:(MEDBeaconPoint *)point
{
    float distance;
    distance = [MEDBeaconPoint lengthFrom:self.position to:point.position];
    [self.processingGraph setValue:@(distance) forKey:[NSString stringWithFormat:@"%d", point.pointId]];
    [point.processingGraph setValue:@(distance) forKey:[NSString stringWithFormat:@"%d", self.pointId]];
}

- (void)removeFromGraphWithPoint:(MEDBeaconPoint *)point
{
    [self.processingGraph removeObjectForKey:[NSString stringWithFormat:@"%d", point.pointId]];
    [point.processingGraph removeObjectForKey:[NSString stringWithFormat:@"%d", self.pointId]];
}

+ (float) lengthFrom:(CGPoint) a to:(CGPoint) b
{
    float length;
    
    length = sqrtf( powf((a.x - b.x), 2.0) +  powf((a.y - b.y), 2.0));
    
    return length;
}

+ (UInt16)idForPointInPointViews:(NSArray *)pointViews
{
    UInt16 pointId;
    
    pointId = 0;
    BOOL existed = NO;
    do {
        pointId++;
        existed = [MEDBeaconPoint pointID:pointId existedInArray:pointViews];
    } while (existed);
    
    return pointId;
}

+ (BOOL)pointID:(UInt16)poinID existedInArray:(NSArray *)array
{
    BOOL existed = false;
    for (ADPointView *pointView in  array) {
        if (poinID == pointView.point.pointId) {
            existed = true;
        }
    }
    return existed;
}


@end
