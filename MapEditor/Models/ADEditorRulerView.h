//
//  ADEditorRulerView.h
//  MapEditor
//
//  Created by Dat Truong on 4/29/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADEditorRulerView : UIView

- (void) updateWithScale:(NSUInteger) scale;

@end
