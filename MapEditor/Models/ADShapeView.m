//
//  ADShapeView.m
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ADShapeView.h"

@interface ADShapeView()

@end

@implementation ADShapeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _furniture = [[MEDFurniture alloc] init];
    }
    return self;
}


+ (UInt16)idForShapeInShapeViews:(NSArray *)shapeViews
{
    UInt16 shapeId;
    
    shapeId = 0;
    BOOL existed = NO;
    do {
        shapeId++;
        existed = [ADShapeView shapeID:shapeId existedInArray:shapeViews];
    } while (existed);
    
    return shapeId;
}

+ (BOOL)shapeID:(UInt16)shapeID existedInArray:(NSArray *)array
{
    BOOL existed = false;
    for (ADShapeView *shapeView in  array) {
        if (shapeID == shapeView.furniture.furnitureId) {
            existed = true;
        }
    }
    return existed;
}


@end
