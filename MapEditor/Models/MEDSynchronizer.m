//
//  MEDSynchronizer.m
//  MapEditor
//
//  Created by El Desperado on 7/23/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "MEDSynchronizer.h"
#import "NSMutableURLRequest+Multipart.h"
#import "NSDictionary+Verified.h"

#define WEB_SERVER_URL @"http://spms.sfone095.com"
//#define WEB_SERVER_URL @"http://192.168.43.130/web/"
#define ERROR_DOMAIN @"com.spms.ErrorDomain"

@implementation MEDSynchronizer
+ (MEDSynchronizer*)sharedService {
  static MEDSynchronizer* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
    sharedService.client = [NSURLSession
        sessionWithConfiguration:[NSURLSessionConfiguration
                                         defaultSessionConfiguration]
                        delegate:sharedService
                   delegateQueue:nil];
    sharedService.baseUrl = [NSURL URLWithString:WEB_SERVER_URL];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

- (void)uploadMapDataWithMapId:(NSString*)mapId
                         image:(UIImage*)image
                    completion:(void (^)(NSDictionary *data, NSError *error))completion {
  NSString* relativeString = @"api/map/upload";
  NSData* imageData;
  NSDictionary* parameters;
  if (image) {
    imageData = UIImageJPEGRepresentation(image, 0.6);
    if (imageData) {
      parameters = @{ @"Image" : imageData, @"mapID" : mapId };
    }
  } else {
    parameters = @{ @"mapID" : mapId };
  }

  NSURL* relativeUrl =
      [NSURL URLWithString:relativeString relativeToURL:self.baseUrl];
  NSMutableURLRequest* request =
      [NSMutableURLRequest requestWithURL:relativeUrl];
  [NSMutableURLRequest addMultipartDataWithParameters:parameters
                                         toURLRequest:request];
  [request setHTTPMethod:@"POST"];

  NSURLSessionDataTask* task =
      [self.client dataTaskWithRequest:request
                     completionHandler:^(NSData* data,
                                         NSURLResponse* response,
                                         NSError* error) {
                         if (error) {
                           completion(nil, error);
                         } else {
                           NSError* jsonError;
                           NSDictionary* dict = [NSJSONSerialization
                               JSONObjectWithData:data
                                          options:NSJSONReadingMutableContainers
                                            error:&jsonError];

                           if (jsonError) {
                             completion(nil, jsonError);
                           } else {
                             completion([dict verifiedObjectForKey:@"Success"], nil);
                           }
                         }
                     }];

  [task resume];
}

- (void)addOrUpdateMapDataWithMapId:(NSString*)mapId
                            mapData:(NSString*)mapData
                              alias:(NSString*)alias
                              level:(NSUInteger)level
                         completion:(void (^)(NSDictionary *data, NSError *error))completion {
  NSString* relativeString = @"api/map/addorupdate";
  NSString* params =
      [NSString stringWithFormat:@"mapID=%@&mapData=%@&alias=%@&level=%lu",
                                 mapId,
                                 mapData,
                                 alias,
                                 (unsigned long)level];

  NSURL* relativeUrl =
      [NSURL URLWithString:relativeString relativeToURL:self.baseUrl];
  NSMutableURLRequest* request =
      [NSMutableURLRequest requestWithURL:relativeUrl];
  [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
  [request setHTTPMethod:@"POST"];

  NSURLSessionDataTask* task =
      [self.client dataTaskWithRequest:request
                     completionHandler:^(NSData* data,
                                         NSURLResponse* response,
                                         NSError* error) {
                         if (error) {
                             completion(nil, error);
                         } else {
                           NSError* jsonError;
                           NSDictionary* dict = [NSJSONSerialization
                               JSONObjectWithData:data
                                          options:NSJSONReadingMutableContainers
                                            error:&jsonError];

                           if (jsonError) {
                             completion(nil, jsonError);
                           } else {
                             completion([dict verifiedObjectForKey:@"Result"], nil);
                           }
                         }
                     }];

  [task resume];
}

- (void)downloadMapDataThenCompletion:(void (^)(NSDictionary *data, NSError *error))completion {
  NSString* relativeString = @"api/map/list";

  NSURL* relativeUrl =
      [NSURL URLWithString:relativeString relativeToURL:self.baseUrl];
  NSMutableURLRequest* request =
      [NSMutableURLRequest requestWithURL:relativeUrl];

  NSURLSessionDataTask* task =
      [self.client dataTaskWithRequest:request
                     completionHandler:^(NSData* data,
                                         NSURLResponse* response,
                                         NSError* error) {
                         if (error) {
                           completion(nil, error);
                         } else {
                           NSError* jsonError;
                           NSDictionary* dict = [NSJSONSerialization
                               JSONObjectWithData:data
                                          options:NSJSONReadingMutableContainers
                                            error:&jsonError];

                           if (jsonError) {
                             completion(nil, jsonError);
                           } else {
                             completion([dict verifiedObjectForKey:@"Result"], nil);
                           }
                         }
                     }];

  [task resume];
}

@end
