//
//  ADEditorView.m
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ADEditorView.h"

#define DPI 72.0
// Line Size
#define LINE_WIDTH 0.5
#define NUM_OF_SMALL_LINES 10.0
#define LINE_SPACE DPI/NUM_OF_SMALL_LINES
// Color
#define SMALL_LINE_COLOR [UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1.0]
#define BIG_LINE_COLOR [UIColor grayColor]

@implementation ADEditorView

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSUInteger numOfHorizontalLines, numOfVerticalLines;
        numOfHorizontalLines = frame.size.height / DPI;
        numOfVerticalLines = frame.size.width / DPI;
        
        [self drawSmallLinesInFrame:frame
                 numOfVerticalLines:numOfVerticalLines
               numOfHorizontalLines:numOfHorizontalLines];
        [self drawBigLinesInFrame:frame
               numOfVerticalLines:numOfVerticalLines
             numOfHorizontalLines:numOfHorizontalLines];
    }
    return self;
}

#pragma mark - Small Lines

- (void)drawSmallVerticalLinesInFrame:(CGRect)frame
                   numOfVerticalLines:(NSUInteger)numOfVerticalLines
{
    for (NSUInteger i = 0; i < numOfVerticalLines; i++) {
        for (NSUInteger j = 0; j < 9; j++) {
            CGRect rect = CGRectMake(i * DPI + (j + 1) * LINE_SPACE - LINE_WIDTH / 2,
                                     0,
                                     LINE_WIDTH,
                                     frame.size.height);
            [self addSubViewWithRect:rect backgroundColor:SMALL_LINE_COLOR];
        }
    }
}

- (void)drawSmallHorizontalLinesInFrame:(CGRect)frame numOfHorizontalLines:(NSUInteger)numOfHorizontalLines
{
    for (NSUInteger i = 0; i < numOfHorizontalLines; i++) {
        for (NSUInteger j = 0; j < 9; j++) {
            CGRect rect = CGRectMake(0,
                                     (i) * DPI - LINE_WIDTH + (j + 1) * LINE_SPACE - LINE_WIDTH / 2,
                                     frame.size.width,
                                     LINE_WIDTH);
            [self addSubViewWithRect:rect backgroundColor:SMALL_LINE_COLOR];
        }
    }
}

- (void)drawSmallLinesInFrame:(CGRect)frame
           numOfVerticalLines:(NSUInteger)numOfVerticalLines
         numOfHorizontalLines:(NSUInteger)numOfHorizontalLines
{
    [self drawSmallVerticalLinesInFrame:frame numOfVerticalLines:numOfVerticalLines];
    [self drawSmallHorizontalLinesInFrame:frame numOfHorizontalLines:numOfHorizontalLines];
}

#pragma mark - Big Lines

- (void)drawBigVerticalLines:(CGRect)frame numOfVerticalLines:(NSUInteger)numOfVerticalLines
{
    for (NSUInteger i = 0; i < numOfVerticalLines; i++) {
        CGRect rect = CGRectMake((i + 1) * DPI - LINE_WIDTH / 2,
                                 0,
                                 LINE_WIDTH,
                                 frame.size.height);
        [self addSubViewWithRect:rect backgroundColor:BIG_LINE_COLOR];
    }
}

- (void)drawBigHorizontalLines:(CGRect)frame numOfHorizontalLines:(NSUInteger)numOfHorizontalLines
{
    for (NSUInteger i = 0; i < numOfHorizontalLines; i++) {
        CGRect rect = CGRectMake(0,
                                 (i + 1) * DPI - LINE_WIDTH / 2,
                                 frame.size.width,
                                 LINE_WIDTH);
        [self addSubViewWithRect:rect backgroundColor:BIG_LINE_COLOR];
    }
}

- (void)drawBigLinesInFrame:(CGRect)frame
         numOfVerticalLines:(NSUInteger)numOfVerticalLines
       numOfHorizontalLines:(NSUInteger)numOfHorizontalLines
{
    [self drawBigVerticalLines:frame numOfVerticalLines:numOfVerticalLines];
    
    [self drawBigHorizontalLines:frame numOfHorizontalLines:numOfHorizontalLines];
}

#pragma mark - Common Drawing function

- (void) addSubViewWithRect:(CGRect) rect backgroundColor:(UIColor *) color
{
    UIView *rectView = [[UIView alloc] initWithFrame:rect];
    [rectView setBackgroundColor:color];
    [self addSubview:rectView];
}

@end

