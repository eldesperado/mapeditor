//
//  ADElipseView.h
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADElipseView : UIView

@property (strong, nonatomic) UIColor *color;

- (instancetype)initWithFrame:(CGRect)frame withColor:(UIColor *) color;

@end
