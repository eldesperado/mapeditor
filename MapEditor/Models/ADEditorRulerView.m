//
//  ADEditorRulerView.m
//  MapEditor
//
//  Created by Dat Truong on 4/29/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ADEditorRulerView.h"

@implementation ADEditorRulerView

- (instancetype)init
{
    self = [super initWithFrame:CGRectMake(12.5, 625.0, 10.0, 50.0)];
    if (self) {
        self.backgroundColor = [UIColor greenColor];
    }
    return self;
}



@end
