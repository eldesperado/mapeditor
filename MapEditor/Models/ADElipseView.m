//
//  ADElipseView.m
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ADElipseView.h"

@implementation ADElipseView

- (instancetype)initWithFrame:(CGRect)frame withColor:(UIColor *) color
{
    self = [self initWithFrame:frame];
    if (self) {
        _color = color;
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGRect bounds = self.bounds;

    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;

    // The circle will be the largest that will fit in the view
    float radius = (MIN(bounds.size.width, bounds.size.height) / 2.0);
    UIBezierPath *path = [[UIBezierPath alloc] init];

    // Add an arc to the path at center, with radius of radius,
    // from 0 to 2*PI radians (a circle)
    [path addArcWithCenter:center
                    radius:radius
                startAngle:0.0
                  endAngle:M_PI * 2.0
                 clockwise:YES];
    UIColor *circleColor;
    circleColor = _color?_color:[UIColor lightGrayColor];
//    if (_color) {
//        circleColor = _color;
//    } else {
//        
//    }
//    [[UIColor lightGrayColor] setFill];
    [circleColor setFill];
    // Draw the line!
    [path fill];
}


@end
