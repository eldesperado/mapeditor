//
//  ADShapeView.h
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEDFurniture.h"

@interface ADShapeView : UIView

@property (nonatomic, strong) MEDFurniture *furniture;

+ (UInt16)idForShapeInShapeViews:(NSArray *)shapeViews;

@end
