//
//  ViewController.m
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ViewController.h"
#import "ADShapeView.h"
#import "ADEditorView.h"
#import "ADEditorRulerView.h"
#import "ADLineView.h"
#import "ADPointView.h"

#import <UIImage-ResizeMagick/UIImage+ResizeMagick.h>

#import <WYPopoverController.h>
#import "PointSettingsViewController.h"
#import "SettingsViewController.h"
#import "StencilCategoryViewController.h"

#import "MEDImageExporter.h"
#import "MEDSynchronizer.h"
#import "MEDJsonGenerator.h"
#import <SVProgressHUD.h>

#define DPI 72.0
#define DEFAULT_SHAPE_WIDTH 200.0
#define DEFAULT_SHAPE_HEIGHT 150.0

#define kM_FURNITURES @"furnitures"
#define kM_POINTS @"points"

typedef NS_ENUM(NSUInteger, ADEditMode) {
  ADModeHand = 0,
  ADModeRoom,
  ADModeShape,
  ADModePoint,
  ADModeLine,
  ADModeText
};

@interface ViewController ()<ADPointViewDelegate,
                             SettingsViewControllerDelegate,
                             WYPopoverControllerDelegate,
                             StencilCategoryViewControllerDelegate,
                             UIImagePickerControllerDelegate,
                             UINavigationControllerDelegate,
                             PointSettingsViewControllerDelegate> {
  SPUserResizableView* currentStencilView;
  UILabel* widthLabel;
  UILabel* heightLabel;
  UILabel* xCoordinateLabel;
  UILabel* yCoordinateLabel;
}

@property float floorWidth;
@property float floorHeight;

// Views
@property(strong, nonatomic) ADEditorView* editorView;
@property(strong, nonatomic) ADEditorRulerView* rulerView;

@property(strong, nonatomic) UIView* pointsView;
@property(strong, nonatomic) UIView* shapesView;
@property(strong, nonatomic) UIView* linesView;

@property(strong, nonatomic) ADPointView* lastEditingPoint;
@property(strong, nonatomic) ADPointView* currentlyEdittingPoint;
@property(strong, nonatomic) ADLineView* currentlyEdittingLine;
@property(strong, nonatomic)
    SPUserResizableView* currentEdittingUserResizableView;

// Controllers
@property(nonatomic, strong) WYPopoverController* shapeMenuPopover;
@property(nonatomic, strong) WYPopoverController* pointSettingsPopover;
@property(nonatomic, strong) WYPopoverController* stencilCategoryMenuPopover;
@property(nonatomic, strong) SettingsViewController* settingsController;
@property(nonatomic, strong) UIPopoverController* settingsPopover;
@property(nonatomic, strong)
    PointSettingsViewController* pointSettingsController;
@property(nonatomic) UIImagePickerController* imagePickerController;

// Objects
@property(strong, nonatomic) NSMutableArray* shapes;
@property(strong, nonatomic) NSMutableArray* points;
@property(strong, nonatomic) NSMutableArray* lineViews;

@end

@implementation ViewController
//- (BOOL)prefersStatusBarHidden {
//  return YES;
//}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self initValues];
  [[self navigationController] setNavigationBarHidden:YES animated:NO];

  self.modeSegment.contentMode = UIViewContentModeScaleAspectFit;
  for (int ii = 0; ii < self.modeSegment.numberOfSegments; ++ii) {
    UIImage* img = [self.modeSegment imageForSegmentAtIndex:ii];
    img = [img resizedImageByMagick:@"20x20#"];
    ;
    [self.modeSegment setImage:img forSegmentAtIndex:ii];
  }

  [self loadMapdata];
  /*

   CGRect imageFrame = CGRectMake(50, 200, 200, 200);
   SPUserResizableView *elipseResizableView = [[SPUserResizableView alloc]
   initWithFrame:imageFrame];
   ADElipseView *elipseView = [[ADElipseView alloc] initWithFrame:imageFrame];
   [elipseResizableView.contentView setBackgroundColor:[UIColor clearColor]];
   elipseResizableView.contentView = elipseView;
   elipseResizableView.delegate = self;
   [self.shapesView addSubview:elipseResizableView];
   [_shapes addObject:elipseResizableView];
   */
}

- (void)loadMapdata {
    [SVProgressHUD showWithStatus:@"Downloading" maskType:SVProgressHUDMaskTypeBlack];
  [[MEDSynchronizer sharedService]
      downloadMapDataThenCompletion:^(NSDictionary* dictionary, NSError *downloadError) {
          if (dictionary) {
              NSArray* mapList = (NSArray*)dictionary;
              NSDictionary* firstMap = [mapList firstObject];
              NSString* mapData;
              
              NSArray* jsonPoints, *jsonFurnitures;
              
              mapData = firstMap[@"mapData"];
              
              NSDictionary* mapDataDict;
              mapDataDict = [NSJSONSerialization
                             JSONObjectWithData:[mapData
                                                 dataUsingEncoding:NSUTF8StringEncoding]
                             options:0
                             error:nil];
              
              jsonFurnitures = mapDataDict[kM_FURNITURES];
              jsonPoints = mapDataDict[kM_POINTS];
              
              MEDJsonGenerator* generator;
              generator = [[MEDJsonGenerator alloc] init];
              
              for (NSDictionary* pointDict in jsonPoints) {
                  MEDBeaconPoint* point;
                  point = [generator pointFromDict:pointDict];
                  dispatch_async(dispatch_get_main_queue(),
                                 ^{ [self addPointViewWithPoint:point]; });
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  for (ADPointView* pointView in self.points) {
                      [self addLinesViewForPointView:pointView];
                  }
              });
              
              for (NSDictionary* furnitureDict in jsonFurnitures) {
                  MEDFurniture* furniture;
                  furniture = [generator furnitureFromDict:furnitureDict];
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [self addFurnitureViewWithFurniture:furniture];
                  });
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [SVProgressHUD showSuccessWithStatus:@"Completed"];
              });
              
          } else {
              NSString *errorMessage;
              errorMessage = downloadError.localizedFailureReason;
              dispatch_async(dispatch_get_main_queue(), ^{
                  [SVProgressHUD showErrorWithStatus:errorMessage];
              });
          }
      }];
}

- (void)addLinesViewForPointView:(ADPointView *)pointView
{
    MEDBeaconPoint* point;
    point = pointView.point;
    NSDictionary* graph;
    graph = point.graphDict;
    for (NSString* key in graph.allKeys) {
        MEDBeaconPoint* nextPoint;
        nextPoint = [self pointWithID:[key integerValue]];
        [self drawLineFrom:point.position to:nextPoint.position];
    }
}

- (MEDBeaconPoint*)pointWithID:(UInt16)pointID {
  MEDBeaconPoint* point;

  for (ADPointView* pointView in self.points) {
    if (pointView.point.pointId == pointID) {
      point = pointView.point;
      break;
    }
  }

  return point;
}

- (MEDBeaconPoint*)pointWithPosition:(CGPoint)position {
  MEDBeaconPoint* point;

  for (ADPointView* pointView in self.points) {
    if (CGPointEqualToPoint(pointView.point.position, position)) {
      point = pointView.point;
      break;
    }
  }

  return point;
}

#pragma mark -
- (void)addEditorView {
  float editorWidth, editorHeight;
  editorHeight = self.scrollView.frame.size.height;
  editorWidth = self.scrollView.frame.size.width;

  self.editorView = [[ADEditorView alloc]
      initWithFrame:CGRectMake(0, 0, editorWidth, editorHeight)];
  [self.editorView setBackgroundColor:[UIColor whiteColor]];
  [self.editorView setClipsToBounds:YES];

  UITapGestureRecognizer* gestureRecognizer;
  gestureRecognizer = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(hideEditingHandles)];
  [gestureRecognizer setDelegate:self];
  [self.editorView addGestureRecognizer:gestureRecognizer];
  [self.scrollView addSubview:self.editorView];
}

- (void)addScrollView {
  self.scrollView.delegate = self;
  [self.scrollView setBackgroundColor:[UIColor grayColor]];
  self.scrollView.contentSize = self.editorView.frame.size;
  self.scrollView.maximumZoomScale = 4.0;
  self.scrollView.minimumZoomScale = 1.0;
  [self.scrollView setCanCancelContentTouches:NO];
}

- (void)addPointsView {
  _pointsView = [[UIView alloc] initWithFrame:_editorView.bounds];
  [_editorView addSubview:_pointsView];

  UITapGestureRecognizer* gestureRecognizer;
  gestureRecognizer = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(pointsViewHandleTap:)];
  [gestureRecognizer setDelegate:self];
  [_pointsView addGestureRecognizer:gestureRecognizer];
}

- (void)addShapesView {
  _shapesView = [[UIView alloc] initWithFrame:_editorView.bounds];
  UITapGestureRecognizer* gestureRecognizer;
  gestureRecognizer = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(pointsViewHandleTap:)];
  [_shapesView addGestureRecognizer:gestureRecognizer];
  [_editorView addSubview:_shapesView];
}

- (void)addLinesView {
  _linesView = [[UIView alloc] initWithFrame:_editorView.bounds];
  [_editorView addSubview:_linesView];
}

- (void)initValues {
  _shapes = [NSMutableArray array];
  _points = [NSMutableArray array];
  _lineViews = [NSMutableArray array];

  [self addEditorView];
  [self addScrollView];

  [self addShapesView];
  [self addLinesView];
  [self addPointsView];

  //    self.rulerView = [[ADEditorRulerView alloc] init];
  //    [self.view addSubview:self.rulerView];

  if (!self.imagePickerController) {
    UIImagePickerController* imagePickerController =
        [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle =
        UIModalPresentationCurrentContext;
    imagePickerController.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    self.imagePickerController = imagePickerController;
  }
}

#pragma mark - Shape Edit Menu Popover

- (void)showShapeMenuEditPopoverForm:(id)sender {
  if ([sender isKindOfClass:[SPUserResizableView class]]) {
    SPUserResizableView* userResizableView = (SPUserResizableView*)sender;

    ADShapeView* shapeView;
    shapeView = (ADShapeView*)userResizableView.contentView;

    ShapeMenuPopoverViewController* shapeMenuPopoverViewController =
        [[ShapeMenuPopoverViewController alloc]
            initWithNibName:@"ShapeMenuPopoverView"
                     bundle:nil];
    shapeMenuPopoverViewController.modalInPopover = NO;
    shapeMenuPopoverViewController.shapePopoverDelegate = self;

    CGRect rect = [userResizableView frame];
    shapeMenuPopoverViewController.preferredContentSize = CGSizeMake(320, 368);

    self.shapeMenuPopover = [[WYPopoverController alloc]
        initWithContentViewController:shapeMenuPopoverViewController];
    self.shapeMenuPopover.delegate = self;

    [self.shapeMenuPopover
          presentPopoverFromRect:[userResizableView bounds]
                          inView:userResizableView
        permittedArrowDirections:WYPopoverArrowDirectionAny
                        animated:YES
                         options:WYPopoverAnimationOptionFadeWithScale];
    [self updateShapeEditMenuForm:shapeMenuPopoverViewController
                             rect:rect
                             name:shapeView.furniture.name];
  }
}

- (void)updateShapeEditMenuForm:(id)sender
                           rect:(CGRect)rect
                           name:(NSString*)name {
  if ([sender isKindOfClass:[ShapeMenuPopoverViewController class]]) {
    ShapeMenuPopoverViewController* popoverVC =
        (ShapeMenuPopoverViewController*)sender;
    popoverVC.widthTextField.text =
        [NSString stringWithFormat:@"%.1f", rect.size.width];
    popoverVC.heightTextField.text =
        [NSString stringWithFormat:@"%.1f", rect.size.height];
    popoverVC.xTextField.text =
        [NSString stringWithFormat:@"%.1f", rect.origin.x];
    popoverVC.yTextField.text =
        [NSString stringWithFormat:@"%.1f", rect.origin.y];
    popoverVC.nameTextField.text = name;
  }
}

#pragma mark - Point Editing Popover

- (void)showPointEditingPopover:(id)sender {
  if ([sender isKindOfClass:[ADPointView class]]) {
    ADPointView* pointView = (ADPointView*)sender;
    PointSettingsViewController* pointSettingsVC =
        [[PointSettingsViewController alloc]
            initWithNibName:@"PointSettingsViewController"
                     bundle:nil];
    pointSettingsVC.modalInPopover = NO;
    pointSettingsVC.delegate = self;

    CGRect rect = [pointView frame];
    pointSettingsVC.preferredContentSize = CGSizeMake(320, 324);

    self.pointSettingsPopover = [[WYPopoverController alloc]
        initWithContentViewController:pointSettingsVC];
    self.pointSettingsPopover.delegate = self;

    [self.pointSettingsPopover
          presentPopoverFromRect:[pointView bounds]
                          inView:pointView
        permittedArrowDirections:WYPopoverArrowDirectionAny
                        animated:YES
                         options:WYPopoverAnimationOptionFadeWithScale];
    [self updatePointEditingPopover:pointSettingsVC rect:rect];
  }
}

- (void)updatePointEditingPopover:(id)sender rect:(CGRect)rect {
  if ([sender isKindOfClass:[PointSettingsViewController class]]) {
    PointSettingsViewController* popoverVC =
        (PointSettingsViewController*)sender;
    popoverVC.xTextField.text =
        [NSString stringWithFormat:@"%.1f", rect.origin.x];
    popoverVC.yTextField.text =
        [NSString stringWithFormat:@"%.1f", rect.origin.y];
    popoverVC.minorTextField.text = [NSString
        stringWithFormat:@"%d", self.currentlyEdittingPoint.point.minor];
    popoverVC.majorTextField.text = [NSString
        stringWithFormat:@"%d", self.currentlyEdittingPoint.point.major];
  }
}

#pragma mark - Point Editing Popover Delegate

- (void)changedWithX:(CGFloat)x
                   y:(CGFloat)y
               minor:(UInt16)minor
               major:(UInt16)major {
  if (self.currentlyEdittingPoint) {
    self.currentlyEdittingPoint.point.position = CGPointMake(x, y);
    self.currentlyEdittingPoint.point.minor = minor;
    self.currentlyEdittingPoint.point.major = major;

    [self.pointSettingsPopover dismissPopoverAnimated:YES];
  }
}

#pragma mark - Shape Edit Menu Popover Delegate
- (void)shapeDataChangedWithWidth:(CGFloat)width
                           height:(CGFloat)height
                                x:(CGFloat)x
                                y:(CGFloat)y {
  if (self.currentEdittingUserResizableView) {
    [self.currentEdittingUserResizableView
        setFrame:CGRectMake(x, y, width, height)];
    [self.currentEdittingUserResizableView layoutIfNeeded];
    [self.shapeMenuPopover dismissPopoverAnimated:YES];
  }
}

- (void)shapeDataChangedWithRect:(CGRect)rect name:(NSString*)name {
  if (self.currentEdittingUserResizableView) {
    [self.currentEdittingUserResizableView setFrame:rect];

    ADShapeView* shapeView;
    shapeView = (ADShapeView*)self.currentEdittingUserResizableView.contentView;
    shapeView.furniture.name = name;
    [self.currentEdittingUserResizableView layoutIfNeeded];
    [self.shapeMenuPopover dismissPopoverAnimated:YES];
  }
}

- (void)flipShapeVertical {
  if (self.currentEdittingUserResizableView) {
    ADShapeView* shapeView;
    shapeView = (ADShapeView*)self.currentEdittingUserResizableView.contentView;
    MEDFurniture* furniture;
    furniture = shapeView.furniture;
    furniture.flippedVertical = !furniture.flippedVertical;

    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                         self.currentEdittingUserResizableView.contentView
                             .transform = CGAffineTransformMakeScale(1, -1);
                     }
                     completion:nil];
  }
}

- (void)flipShapeHorizontal {
  if (self.currentEdittingUserResizableView) {
    ADShapeView* shapeView;
    shapeView = (ADShapeView*)self.currentEdittingUserResizableView.contentView;
    MEDFurniture* furniture;
    furniture = shapeView.furniture;
    furniture.flippedHorizontal = !furniture.flippedHorizontal;

    CALayer* layer = self.currentEdittingUserResizableView.contentView.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
    rotationAndPerspectiveTransform = CATransform3DRotate(
        rotationAndPerspectiveTransform, M_PI / 0.3, 0.0f, 1.0f, 0.0f);
    layer.transform = rotationAndPerspectiveTransform;
    [UIView animateWithDuration:1.0
                     animations:^{ layer.transform = CATransform3DIdentity; }];
  }
}

- (void)deleteShape {
  if (self.currentEdittingUserResizableView) {
      [self.shapes removeObject:self.currentEdittingUserResizableView];
    [self.currentEdittingUserResizableView removeFromSuperview];
    [self.shapeMenuPopover dismissPopoverAnimated:YES];
  }
}

#pragma mark - IBActions

- (IBAction)showSavePopover:(id)sender {
  if (_settingsController == nil) {
    _settingsController =
        [[SettingsViewController alloc] initWithStyle:UITableViewStylePlain];
    _settingsController.delegate = self;
  }

  if (_settingsPopover == nil) {
    _settingsPopover = [[UIPopoverController alloc]
        initWithContentViewController:_settingsController];
    [_settingsPopover presentPopoverFromRect:[(UIButton*)sender frame]
                                      inView:self.view
                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                    animated:YES];
  } else {
    [_settingsPopover dismissPopoverAnimated:YES];
    _settingsPopover = nil;
  }
}

- (IBAction)removeShape:(id)sender {
  if (currentlyEditingView) {
    [currentlyEditingView removeFromSuperview];
    if ([_shapes containsObject:currentlyEditingView]) {
      [_shapes removeObject:currentlyEditingView];
    }
  }
}

- (IBAction)resetPoints:(id)sender {
  for (UIView* pointView in [_pointsView subviews]) {
    [pointView removeFromSuperview];
  }
  [_points removeAllObjects];
}

#pragma mark Private UI Functions

- (void)addPointAt:(CGPoint)position {
  // Create ADPoint, add it to _points
  MEDBeaconPoint* point = [[MEDBeaconPoint alloc] init];
  point.position = position;
  point.pointId = [MEDBeaconPoint idForPointInPointViews:self.points];

  ADPointView* pointView = [[ADPointView alloc] initWithPoint:point];
  ;
  pointView.delegate = self;
  [_points addObject:pointView];
  [_pointsView addSubview:pointView];
}

- (void)addPointViewWithPoint:(MEDBeaconPoint*)pointToAdd {
  MEDBeaconPoint* point;
  point = pointToAdd;

  ADPointView* pointView = [[ADPointView alloc] initWithPoint:point];
  ;
  pointView.delegate = self;
  [_points addObject:pointView];
  [_pointsView addSubview:pointView];
}

- (void)drawLine {
  if (self.lastEditingPoint && self.currentlyEdittingPoint &&
      self.lastEditingPoint != self.currentlyEdittingPoint) {
    [self drawLineFrom:self.lastEditingPoint.point.position
                    to:self.currentlyEdittingPoint.point.position];
    [self.lastEditingPoint.point
        addToGraphWithPoint:self.currentlyEdittingPoint.point];
  }
}

- (void)drawLineFrom:(CGPoint)a to:(CGPoint)b {
  ADLineView* lineView = [[ADLineView alloc] initWithPoint:a to:b];
  if (lineView) {
    BOOL lineExisted;
    lineExisted = [self line:lineView existInLines:self.lineViews];
    if (!lineExisted) {
      [_linesView addSubview:lineView];
      [_lineViews addObject:lineView];
    }
  }
}

- (BOOL)line:(ADLineView*)line existInLines:(NSArray*)lines {
  BOOL existed;

  existed = NO;
  for (ADLineView* lineView in lines) {
    if ((CGPointEqualToPoint(line.startPoint, lineView.startPoint) &&
         CGPointEqualToPoint(line.endPoint, lineView.endPoint)) ||
        (CGPointEqualToPoint(line.endPoint, lineView.startPoint) &&
         CGPointEqualToPoint(line.startPoint, lineView.endPoint))) {
      existed = YES;
      break;
    }
  }

  return existed;
}

- (void)updateCurrentInfo {
  if (currentlyEditingView) {
    float currentX, currentY, currentWidth, currentHeight;
    CGRect currentEditFrame, currentContentFrame;
    currentEditFrame = currentlyEditingView.frame;
    currentContentFrame = currentlyEditingView.contentView.frame;

    currentX = (currentEditFrame.origin.x + currentContentFrame.origin.x) / DPI;
    currentY = (_editorView.frame.size.height - currentEditFrame.origin.y -
                currentEditFrame.size.height + currentContentFrame.origin.x) /
               DPI;
    currentWidth = currentContentFrame.size.width / DPI;
    currentHeight = currentContentFrame.size.height / DPI;

    _xTextField.text = [NSString stringWithFormat:@"%.2f m", currentX];
    _yTextField.text = [NSString stringWithFormat:@"%.2f m", currentY];
    _widthTextField.text = [NSString stringWithFormat:@"%.2f m", currentWidth];
    _heightTextField.text =
        [NSString stringWithFormat:@"%.2f m", currentHeight];
  }
}

- (void)clearCurrentInfo {
  _xTextField.text = @"";
  _yTextField.text = @"";
  _widthTextField.text = @"";
  _heightTextField.text = @"";
}

#pragma mark - UIScrollViewDelegate

- (UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView {
  return self.editorView;
}

- (void)scrollViewDidZoom:(UIScrollView*)scrollView {
  UIView* subView = [scrollView.subviews objectAtIndex:0];

  CGFloat offsetX =
      (scrollView.bounds.size.width > scrollView.contentSize.width)
          ? (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5
          : 0.0;

  CGFloat offsetY =
      (scrollView.bounds.size.height > scrollView.contentSize.height)
          ? (scrollView.bounds.size.height - scrollView.contentSize.height) *
                0.5
          : 0.0;

  subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                               scrollView.contentSize.height * 0.5 + offsetY);
}

#pragma mark - SPUserResizableViewDelegate

- (void)userResizableViewDidBeginEditing:
            (SPUserResizableView*)userResizableView {
  [currentlyEditingView hideEditingHandles];
  currentlyEditingView = userResizableView;

  // Display Shape Property Info
  [self addShapePropertyInfoViewWithShapeRect:userResizableView.frame];
  [self updateCurrentInfo];
}

- (void)userResizableViewDidEndEditing:(SPUserResizableView*)userResizableView {
  lastEditedView = userResizableView;
}

- (void)userResizableViewdDoubleTapped:(SPUserResizableView*)userResizableView {
  self.currentEdittingUserResizableView = userResizableView;
  [self showShapeMenuEditPopoverForm:userResizableView];

  // Remove Shape Property Info
  [self removeShapePropertyInfoViewWithShape];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer
       shouldReceiveTouch:(UITouch*)touch {
  if ([currentlyEditingView hitTest:[touch locationInView:currentlyEditingView]
                          withEvent:nil]) {
    return NO;
  }
  return YES;
}

#pragma mark - UIGesture
- (void)pointsViewHandleTap:(UITapGestureRecognizer*)paramTap {
  CGPoint tapPoint = [paramTap locationInView:_pointsView];
  switch (self.modeSegment.selectedSegmentIndex) {
    case ADModeHand:
//      [self deselectAll];
          [self hideEditingHandles];
      break;
    case ADModeRoom:
      [self openRoomMenuAction:self];
      break;
    case ADModeShape:
      [self openStencilMenuAction:self];
      break;
    case ADModePoint: {
      ADPointView* pointViewNearBy;
      pointViewNearBy = [self pointNearBy:tapPoint];
      if (pointViewNearBy) {
        [self didSetSelected:pointViewNearBy];
        [self showPointEditingPopover:pointViewNearBy];
      } else {
        [self addPointAt:tapPoint];
      }

    } break;
    case ADModeLine: {
      ADPointView* pointViewNearBy;
      pointViewNearBy = [self pointNearBy:tapPoint];
      if (!pointViewNearBy || pointViewNearBy == self.currentlyEdittingPoint) {
        [self deselectPoint];

        ADLineView* lineViewNearBy;
        lineViewNearBy = [self lineNearBy:tapPoint];

        if (self.currentlyEdittingLine) {
          self.currentlyEdittingLine.isSelected = NO;
        }

        if (lineViewNearBy) {
          self.currentlyEdittingLine = lineViewNearBy;
          self.currentlyEdittingLine.isSelected = YES;

          [self becomeFirstResponder];

          UIMenuController* menu = [UIMenuController sharedMenuController];

          UIMenuItem* menuItem =
              [[UIMenuItem alloc] initWithTitle:@"Delete"
                                         action:@selector(deleteCurrentLine:)];
          menu.menuItems = @[ menuItem ];

          CGPoint centerPoint;
          centerPoint = [self centerPointOfLine:lineViewNearBy];
          [menu setTargetRect:CGRectMake(centerPoint.x, centerPoint.y, 2, 2)
                       inView:self.editorView];
          [menu setMenuVisible:YES animated:YES];

        } else {
        }
      } else {
        [pointViewNearBy selectPoint];
      }
    } break;
    default:
      break;
  }
  if (self.modeSegment.selectedSegmentIndex == ADModePoint) {
  }
}

- (IBAction)deleteCurrentLine:(id)sender {
  ADLineView* lineToDelete;
  lineToDelete = self.currentlyEdittingLine;

  [self deleteLine:lineToDelete
   willDeleteGraph:YES];
}

- (void)deleteLine:(ADLineView*)lineToDelete
       willDeleteGraph:(BOOL)willDeleteGraph
{
  MEDBeaconPoint* startPoint, *endPoint;
    if (willDeleteGraph) {
        startPoint = [self pointWithPosition:lineToDelete.startPoint];
        endPoint = [self pointWithPosition:lineToDelete.endPoint];
    }

  [startPoint removeFromGraphWithPoint:endPoint];

  [lineToDelete removeFromSuperview];
  [self.lineViews removeObject:lineToDelete];
}

- (BOOL)canBecomeFirstResponder {
  return YES;
}

- (void)deleteCurrentPoint {
  ADPointView* pointToDelete;
  pointToDelete = self.currentlyEdittingPoint;
  [self deletePoint:pointToDelete];
  [self.pointSettingsPopover dismissPopoverAnimated:YES];
}

- (void)deletePoint:(ADPointView*)pointToDelete {
  // delete point related elements
    [self deleteRelatedElementsToPointView:pointToDelete willDeleteGraph:YES];
  // delete point
  [self.currentlyEdittingPoint removeFromSuperview];
  [self.points removeObject:self.currentlyEdittingPoint];
}

- (void)deleteRelatedElementsToPointView:(ADPointView *)pointViewToDelete
                         willDeleteGraph:(BOOL)willDeleteGraph
{
    MEDBeaconPoint* point;
    point = pointViewToDelete.point;
    for (NSString* endPointIdString in point.graphDict.allKeys) {
        UInt16 startPointId, endPointId;
        startPointId = point.pointId;
        endPointId = [endPointIdString integerValue];
        //    NSLog(@"start: %d end: %d", startPointId, endPointId);
        ADLineView* lineToDelete;
        lineToDelete =
        [self lineViewWithStartPointID:startPointId endPoint:endPointId];
        [self deleteLine:lineToDelete willDeleteGraph:willDeleteGraph];
    }
}

- (ADLineView*)lineViewWithStartPoint:(CGPoint)startPoint
                             endPoint:(CGPoint)endPoint {
  ADLineView* lineView;

  for (ADLineView* line in self.lineViews) {
    if ((CGPointEqualToPoint(startPoint, line.startPoint) &&
         CGPointEqualToPoint(endPoint, line.endPoint)) ||
        (CGPointEqualToPoint(endPoint, line.startPoint) &&
         CGPointEqualToPoint(startPoint, line.endPoint))) {
      lineView = line;
      break;
    }
  }

  return lineView;
}

- (ADLineView*)lineViewWithStartPointID:(UInt16)startPointID
                               endPoint:(UInt16)endPointID {
  ADLineView* lineView;

  MEDBeaconPoint* startPoint, *endPoint;
  startPoint = [self pointWithID:startPointID];
  endPoint = [self pointWithID:endPointID];
  lineView = [self lineViewWithStartPoint:startPoint.position
                                 endPoint:endPoint.position];

  return lineView;
}

- (ADPointView*)pointNearBy:(CGPoint)point {
  ADPointView* pointViewNearBy;

  for (ADPointView* pointView in self.points) {
    float length =
        [MEDBeaconPoint lengthFrom:point to:pointView.point.position];
    if (length < 20.0) {
      pointViewNearBy = pointView;
      break;
    }
  }

  return pointViewNearBy;
}

- (ADLineView*)lineNearBy:(CGPoint)point {
  ADLineView* lineViewNearBy;

  for (ADLineView* lineView in self.lineViews) {
    float lineLength;
    CGPoint centerLinePoint;
    centerLinePoint = [self centerPointOfLine:lineView];
    lineLength = [MEDBeaconPoint lengthFrom:point to:centerLinePoint];
    if (lineLength < 20.0) {
      lineViewNearBy = lineView;
      break;
    }
  }

  return lineViewNearBy;
}

- (CGPoint)centerPointOfLine:(ADLineView*)lineView {
  CGPoint centerLinePoint;
  centerLinePoint =
      CGPointMake((lineView.startPoint.x + lineView.endPoint.x) / 2.0,
                  (lineView.startPoint.y + lineView.endPoint.y) / 2.0);
  return centerLinePoint;
}

- (void)deselectPoint {
  if (self.currentlyEdittingPoint) {
    self.currentlyEdittingPoint.isSelected = NO;
    self.currentlyEdittingPoint = nil;
    self.lastEditingPoint = nil;
  }
}

- (void)hideEditingHandles {
  // We only want the gesture recognizer to end the editing session on the last
  // edited view. We wouldn't want to dismiss an editing session in progress.
  [lastEditedView hideEditingHandles];
  [self clearCurrentInfo];
}

- (void)touchMoved {
  // Display Shape Property Info
  ADShapeView* shapeView;
  shapeView = (ADShapeView*)currentlyEditingView.contentView;
  MEDFurniture* furniture;
  furniture = shapeView.furniture;
  furniture.rect = currentlyEditingView.frame;

  [self addShapePropertyInfoViewWithShapeRect:currentlyEditingView.frame];
  [self updateCurrentInfo];
}

#pragma mark - ADPointViewDelegate

- (void)didSetSelected:(ADPointView*)currentPoint {
  self.lastEditingPoint = self.currentlyEdittingPoint;
  if (self.lastEditingPoint) {
    self.lastEditingPoint.isSelected = NO;
  }
  self.currentlyEdittingPoint = currentPoint;

  if (self.modeSegment.selectedSegmentIndex == ADModeLine) {
    [self drawLine];
  }
}

- (IBAction)modeDidChange:(UISegmentedControl*)sender {
  [self deselectAll];
  if (sender == self.modeSegment) {
    switch (sender.selectedSegmentIndex) {
      case ADModeRoom:
        [self openRoomMenuAction:sender];
        [self.modeSegment setSelectedSegmentIndex:0];
        [_linesView setUserInteractionEnabled:NO];
        [_pointsView setUserInteractionEnabled:NO];
        break;
      case ADModeShape:
        [self openStencilMenuAction:sender];
        [self.modeSegment setSelectedSegmentIndex:0];
        [_linesView setUserInteractionEnabled:NO];
        [_pointsView setUserInteractionEnabled:NO];
        break;
      case ADModeLine:
      case ADModePoint:
        [_linesView setUserInteractionEnabled:NO];
        [_pointsView setUserInteractionEnabled:YES];
        break;
      default:
        [_linesView setUserInteractionEnabled:NO];
        [_pointsView setUserInteractionEnabled:NO];
        break;
    }
  }
}

/*- (IBAction)testMapSynchronizerAction:(id)sender {
  UIImage* image = [UIImage imageNamed:@"bench"];
  [[MEDSynchronizer sharedService]
      uploadMapDataWithMapId:@"aloha381"
                       image:image
                  completion:^(NSDictionary* dictionary) {
                      NSLog(@"Synchronizer Upload Result: %@", dictionary);
                  }];
  [[MEDSynchronizer sharedService]
      downloadMapDataThenCompletion:^(NSDictionary* dictionary) {
          NSLog(@"Synchronizer Download Result: %@", dictionary);
      }];
  [[MEDSynchronizer sharedService]
      addOrUpdateMapDataWithMapId:@"aloha381"
                          mapData:@"MapData"
                            alias:@"Synchronizer Test Alias"
                            level:1
                       completion:^(NSDictionary* dictionary) {
                           NSLog(@"Synchronizer Add/Update Result: %@",
                                 dictionary);
                       }];
}*/

- (IBAction)openStencilMenuAction:(id)sender {
  if (!self.stencilCategoryMenuPopover) {
    UISegmentedControl* button = (UISegmentedControl*)sender;
    StencilCategoryViewController* stencilCategoryViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:
                             @"stencilCategoryViewController"];
    stencilCategoryViewController.preferredContentSize = CGSizeMake(320, 280);
    stencilCategoryViewController.delegate = self;
    stencilCategoryViewController.title = @"Add Stencil";

    [stencilCategoryViewController.navigationItem
        setLeftBarButtonItem:[[UIBarButtonItem alloc]
                                 initWithTitle:@"Add"
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(addStencilToEditor:)]];

    [stencilCategoryViewController.navigationItem
        setRightBarButtonItem:[[UIBarButtonItem alloc]
                                  initWithTitle:@"Close"
                                          style:UIBarButtonItemStylePlain
                                         target:self
                                         action:@selector(
                                                    closeStencilMenuPopover:)]];

    stencilCategoryViewController.modalInPopover = NO;

    UINavigationController* contentViewController =
        [[UINavigationController alloc]
            initWithRootViewController:stencilCategoryViewController];

    self.stencilCategoryMenuPopover = [[WYPopoverController alloc]
        initWithContentViewController:contentViewController];
    self.stencilCategoryMenuPopover.delegate = self;
    self.stencilCategoryMenuPopover.passthroughViews = @[ button ];
    self.stencilCategoryMenuPopover.popoverLayoutMargins =
        UIEdgeInsetsMake(10, 10, 10, 10);
    self.stencilCategoryMenuPopover.wantsDefaultContentAppearance = NO;

    [self.stencilCategoryMenuPopover
          presentPopoverFromRect:button.bounds
                          inView:button
        permittedArrowDirections:WYPopoverArrowDirectionAny
                        animated:YES
                         options:WYPopoverAnimationOptionFadeWithScale];
  } else {
    [self closeStencilMenuPopover:nil];
  }
}

- (IBAction)openRoomMenuAction:(id)sender {
  [self presentViewController:self.imagePickerController
                     animated:YES
                   completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController*)picker
    didFinishPickingMediaWithInfo:(NSDictionary*)info {
  UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];

  [self setStencilWithImage:image];

  [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker {
  [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - WYPopoverControllerDelegate

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController*)controller {
  return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController*)controller {
  if (controller == self.stencilCategoryMenuPopover) {
    self.stencilCategoryMenuPopover.delegate = nil;
    self.stencilCategoryMenuPopover = nil;
    currentStencilView = nil;
  }
}

- (BOOL)popoverControllerShouldIgnoreKeyboardBounds:
            (WYPopoverController*)popoverController {
  return NO;
}

#pragma mark - Stencil Category Controller Delegate
- (void)didSelectedStencilWithImage:(UIImage*)image andId:(UInt16)stencilId {
  SPUserResizableView* stencilView;

  UIImageView* selectedImageView = [[UIImageView alloc] initWithImage:image];
  stencilView =
      [[SPUserResizableView alloc] initWithFrame:selectedImageView.frame];
  stencilView.userInteractionEnabled = YES;
  ADShapeView* shapeView;

  shapeView = [[ADShapeView alloc] initWithFrame:selectedImageView.frame];
  NSMutableArray* tempShapeViews;
  tempShapeViews = [NSMutableArray array];
  for (SPUserResizableView* userResizeableView in self.shapes) {
    ADShapeView* shapeView = (ADShapeView*)userResizeableView.contentView;
    [tempShapeViews addObject:shapeView];
  }
  UInt16 furId;
  furId = [ADShapeView idForShapeInShapeViews:tempShapeViews];
  shapeView.furniture.furnitureId = furId;
  shapeView.furniture.rect = shapeView.frame;
  shapeView.furniture.stencilId = stencilId;
  //    [shapeView setBackgroundColor:[UIColor colorWithRed:255.0 / 255.0
  //                                                    green:221.0 / 255.0
  //                                                     blue:120.0 / 255.0
  //                                                    alpha:1.0]];
  selectedImageView.autoresizingMask =
      UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
  [shapeView addSubview:selectedImageView];
  stencilView.contentView = shapeView;

  currentStencilView = stencilView;
  currentStencilView.frame = stencilView.frame;
  [self addStencilToEditor:stencilView];
}

- (void)addFurnitureViewWithFurniture:(MEDFurniture*)furniture {
  SPUserResizableView* stencilView;

  UIImage* image;
  image = [UIImage
      imageNamed:[NSString stringWithFormat:@"%hu", furniture.stencilId]];
  UIImageView* selectedImageView = [[UIImageView alloc] initWithImage:image];
  stencilView = [[SPUserResizableView alloc] initWithFrame:furniture.rect];
  stencilView.userInteractionEnabled = YES;
  ADShapeView* shapeView;

  shapeView = [[ADShapeView alloc] initWithFrame:selectedImageView.frame];
  NSMutableArray* tempShapeViews;
  tempShapeViews = [NSMutableArray array];
  for (SPUserResizableView* userResizeableView in self.shapes) {
    ADShapeView* shapeView = (ADShapeView*)userResizeableView.contentView;
    [tempShapeViews addObject:shapeView];
  }
  shapeView.furniture = furniture;
  //    [shapeView setBackgroundColor:[UIColor colorWithRed:255.0 / 255.0
  //                                                    green:221.0 / 255.0
  //                                                     blue:120.0 / 255.0
  //                                                    alpha:1.0]];
  selectedImageView.autoresizingMask =
      UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
  [shapeView addSubview:selectedImageView];
  stencilView.contentView = shapeView;

  currentStencilView = stencilView;
  currentStencilView.frame = stencilView.frame;

  [self addStencilToEditor:stencilView];

  if (furniture.flippedVertical) {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                         currentStencilView.contentView.transform =
                             CGAffineTransformMakeScale(1, -1);
                     }
                     completion:nil];
  }

  if (furniture.flippedHorizontal) {
    CALayer* layer = currentStencilView.contentView.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
    rotationAndPerspectiveTransform = CATransform3DRotate(
        rotationAndPerspectiveTransform, M_PI / 0.3, 0.0f, 1.0f, 0.0f);
    layer.transform = rotationAndPerspectiveTransform;
    [UIView animateWithDuration:1.0
                     animations:^{ layer.transform = CATransform3DIdentity; }];
  }
}

#pragma mark - Settings View Controller Delegate

- (void)save {
  NSLog(@"let's save");
    [SVProgressHUD showWithStatus:@"Uploading" maskType:SVProgressHUDMaskTypeBlack];
  if (_settingsPopover != nil) {
    [_settingsPopover dismissPopoverAnimated:YES];
    _settingsPopover = nil;
  }

  [self exportImage];
  //    [self generateMapdata];

  [[MEDSynchronizer sharedService]
      addOrUpdateMapDataWithMapId:@"1"
                          mapData:[self generateMapdata]
                            alias:@"Floor 1"
                            level:1
                       completion:^(NSDictionary* dictionary, NSError *updateError) {
                           if (dictionary) {
                               NSLog(@"Synchronizer Add/Update Result: %@",
                                     dictionary);
                               
                               NSArray* paths = NSSearchPathForDirectoriesInDomains(
                                                                                    NSDocumentDirectory, NSUserDomainMask, YES);
                               NSString* filePath = [[paths objectAtIndex:0]
                                                     stringByAppendingPathComponent:
                                                     [NSString stringWithFormat:@"%@.jpg",
                                                      @"mapImage"]];
                               
                               UIImage* image =
                               [UIImage imageWithContentsOfFile:filePath];
                               [[MEDSynchronizer sharedService]
                                uploadMapDataWithMapId:@"1"
                                image:image
                                completion:^(NSDictionary*
                                             dictionary, NSError *uploadError) {
                                    if (dictionary) {
                                        NSLog(@"Synchronizer Upload "
                                              @"Result: %@",
                                              dictionary);
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [SVProgressHUD showSuccessWithStatus:@"Completed"];
                                        });
                                    } else {
                                        NSString *errorMessage;
                                        errorMessage = uploadError.localizedFailureReason;
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [SVProgressHUD showErrorWithStatus:errorMessage];
                                        });
                                    }
                                }];
                           } else {
                               NSString *errorMessage;
                               errorMessage = updateError.localizedFailureReason;
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [SVProgressHUD showErrorWithStatus:errorMessage];
                               });
                           }
                       }];
}

- (NSString*)generateMapdata {
  MEDJsonGenerator* generator;
  generator = [[MEDJsonGenerator alloc] init];

  NSString* jsonMapdataString;
  jsonMapdataString = [generator generateMapdataFromPoints:[self.points copy]
                                                    shapes:[self.shapes copy]];

  NSLog(@"mapdata: \n %@", jsonMapdataString);
  return jsonMapdataString;
}

- (void)exportImage {
  [lastEditedView hideEditingHandles];
  MEDImageExporter* imageExporter;
  imageExporter = [[MEDImageExporter alloc] init];
  [imageExporter exportImageFromView:_shapesView withName:@"mapImage"];
}

#pragma mark - Actions
- (void)closeStencilMenuPopover:(id)sender {
  [self.stencilCategoryMenuPopover
      dismissPopoverAnimated:YES
                  completion:^{
                      self.stencilCategoryMenuPopover = nil;
                      currentStencilView = nil;
                      ;
                  }];
}

- (void)setStencilWithImage:(UIImage*)image {
  if (image) {
    SPUserResizableView* userResizableView;
    UIImageView* selectedImageView = [[UIImageView alloc] initWithImage:image];
    userResizableView =
        [[SPUserResizableView alloc] initWithFrame:selectedImageView.frame];
    userResizableView.userInteractionEnabled = YES;
    UIView* contentView =
        [[UIView alloc] initWithFrame:selectedImageView.frame];
    [contentView setBackgroundColor:[UIColor colorWithRed:255.0 / 255.0
                                                    green:221.0 / 255.0
                                                     blue:120.0 / 255.0
                                                    alpha:1.0]];

    userResizableView.contentView = selectedImageView;
    [self addStencilToEditor:userResizableView];
  }
}

- (void)deselectAll {
  [self deselectPoint];
  [self hideEditingHandles];
}

- (void)addStencilToEditor:(id)sender {
  if ([sender isKindOfClass:[SPUserResizableView class]]) {
    SPUserResizableView* view = (SPUserResizableView*)sender;
    view.delegate = self;
    [self.shapesView addSubview:view];
    [_shapes addObject:view];
    [self closeStencilMenuPopover:nil];

    [self.modeSegment setSelectedSegmentIndex:0];
    [_linesView setUserInteractionEnabled:NO];
    [_pointsView setUserInteractionEnabled:NO];
  }
}

- (void)addShapePropertyInfoViewWithShapeRect:(CGRect)shapeRect {
  if (!widthLabel || !heightLabel || !xCoordinateLabel || !yCoordinateLabel) {
    NSUInteger topMargin = 70;
    NSUInteger labelKerning = 10;
    NSUInteger labelGap = 10;
    NSUInteger viewWidth = 100;
    NSUInteger rightMargin = viewWidth + labelGap + 20;
    CGRect rect = CGRectMake(CGRectGetMaxX(self.view.frame) - rightMargin,
                             self.view.frame.origin.y + topMargin,
                             viewWidth,
                             60);
    UIView* shapePropertyInfoView = [[UIView alloc] initWithFrame:rect];
    UIFont* font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
    widthLabel = [[UILabel alloc]
        initWithFrame:CGRectMake(shapePropertyInfoView.frame.origin.x + 4,
                                 shapePropertyInfoView.frame.origin.y + 4,
                                 60,
                                 20)];

    [widthLabel setFont:font];
    [widthLabel
        setTextColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
    heightLabel = [[UILabel alloc]
        initWithFrame:CGRectMake(widthLabel.frame.origin.x +
                                     widthLabel.frame.size.width + labelGap,
                                 widthLabel.frame.origin.y,
                                 60,
                                 20)];

    [heightLabel setFont:font];
    [heightLabel
        setTextColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
    xCoordinateLabel = [[UILabel alloc]
        initWithFrame:CGRectMake(widthLabel.frame.origin.x,
                                 widthLabel.frame.origin.y +
                                     widthLabel.frame.size.height +
                                     labelKerning,
                                 60,
                                 20)];

    [xCoordinateLabel setFont:font];
    [xCoordinateLabel
        setTextColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];
    yCoordinateLabel = [[UILabel alloc]
        initWithFrame:CGRectMake(xCoordinateLabel.frame.origin.x +
                                     xCoordinateLabel.frame.size.width +
                                     labelGap,
                                 xCoordinateLabel.frame.origin.y,
                                 60,
                                 20)];
    [yCoordinateLabel setFont:font];
    [yCoordinateLabel
        setTextColor:
            [UIColor colorWithRed:0.906 green:0.298 blue:0.235 alpha:1.000]];

    [widthLabel
        setText:[NSString stringWithFormat:@"W: %.1f", shapeRect.size.width]];
    [heightLabel
        setText:[NSString stringWithFormat:@"H: %.1f", shapeRect.size.height]];
    [xCoordinateLabel
        setText:[NSString stringWithFormat:@"X: %.1f", shapeRect.origin.x]];
    [yCoordinateLabel
        setText:[NSString stringWithFormat:@"Y: %.1f", shapeRect.origin.y]];

    [self.view addSubview:widthLabel];
    [self.view addSubview:heightLabel];
    [self.view addSubview:xCoordinateLabel];
    [self.view addSubview:yCoordinateLabel];

    [self.view addSubview:widthLabel];
  } else {
    [widthLabel
        setText:[NSString stringWithFormat:@"W: %.1f", shapeRect.size.width]];
    [heightLabel
        setText:[NSString stringWithFormat:@"H: %.1f", shapeRect.size.height]];
    [xCoordinateLabel
        setText:[NSString stringWithFormat:@"X: %.1f", shapeRect.origin.x]];
    [yCoordinateLabel
        setText:[NSString stringWithFormat:@"Y: %.1f", shapeRect.origin.y]];
  }
}

- (void)removeShapePropertyInfoViewWithShape {
  [widthLabel removeFromSuperview];
  [heightLabel removeFromSuperview];
  [xCoordinateLabel removeFromSuperview];
  [yCoordinateLabel removeFromSuperview];
  widthLabel = nil;
  heightLabel = nil;
  xCoordinateLabel = nil;
  yCoordinateLabel = nil;
}


#pragma mark - ADPointView Delegate

- (void)panDidStarted:(ADPointView *)currentPoint
{
    [self deleteRelatedElementsToPointView:currentPoint willDeleteGraph:NO];
}

- (void)panDidEnded:(ADPointView *)currentPoint
{
    currentPoint.point.position = currentPoint.frame.origin;
    [self addLinesViewForPointView:currentPoint];
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)refreshMap:(id)sender {
    [self initValues];
    [self loadMapdata];
}
@end
