//
//  StencilCategoryViewController.h
//  MapEditor
//
//  Created by El Desperado on 7/17/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StencilCategoryViewControllerDelegate;

@interface StencilCategoryViewController : UIViewController

@property(nonatomic, weak) id<StencilCategoryViewControllerDelegate> delegate;

@property(strong, nonatomic) IBOutlet UITableView* tableView;

@end

@protocol StencilCategoryViewControllerDelegate<NSObject>
@required
- (void)didSelectedStencilWithImage:(UIImage*)image andId:(UInt16)stencilId;

@end