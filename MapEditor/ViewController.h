//
//  ViewController.h
//  MapEditor
//
//  Created by Dat Truong on 4/28/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"
#import "ShapeMenuPopoverViewController.h"

@interface ViewController
    : UIViewController<UIScrollViewDelegate,
                       UIGestureRecognizerDelegate,
                       SPUserResizableViewDelegate,
                       ShapeMenuPopoverViewControllerDelegate> {
  SPUserResizableView* currentlyEditingView;
  SPUserResizableView* lastEditedView;
}
@property(weak, nonatomic) IBOutlet UIScrollView* scrollView;
@property(weak, nonatomic) IBOutlet UIView* propertiesView;

@property(weak, nonatomic) IBOutlet UITextField* widthTextField;
@property(weak, nonatomic) IBOutlet UITextField* heightTextField;
@property(weak, nonatomic) IBOutlet UITextField* xTextField;
@property(weak, nonatomic) IBOutlet UITextField* yTextField;
@property(weak, nonatomic) IBOutlet UISegmentedControl* modeSegment;

- (IBAction)modeDidChange:(UISegmentedControl*)sender;
//- (IBAction)testMapSynchronizerAction:(id)sender;

@end
