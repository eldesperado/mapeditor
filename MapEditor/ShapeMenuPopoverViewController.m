//
//  ShapeMenuPopoverViewController.m
//  MapEditor
//
//  Created by El Desperado on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "ShapeMenuPopoverViewController.h"

@interface ShapeMenuPopoverViewController ()

@end

@implementation ShapeMenuPopoverViewController
@synthesize shapePopoverDelegate;

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    self.widthTextField = [[UITextField alloc] init];
    self.heightTextField = [[UITextField alloc] init];
    self.xTextField = [[UITextField alloc] init];
    self.yTextField = [[UITextField alloc] init];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (IBAction)doneAction:(id)sender {
  if (self.shapePopoverDelegate &&
      [self.shapePopoverDelegate
          respondsToSelector:
              @selector(shapeDataChangedWithRect:name:)]) {
          CGRect newRect;
          newRect = CGRectMake([self.xTextField.text floatValue],
                               [self.yTextField.text floatValue],
                               [self.widthTextField.text floatValue],
                               [self.heightTextField.text floatValue]);
          NSString *newName;
          newName = self.nameTextField.text;
    [self.shapePopoverDelegate
     shapeDataChangedWithRect:newRect name:newName];
  }
}

- (IBAction)flipHorizontalAction:(id)sender {
  if (self.shapePopoverDelegate &&
      [self.shapePopoverDelegate
          respondsToSelector:@selector(flipShapeHorizontal)]) {
    [self.shapePopoverDelegate flipShapeHorizontal];
  }
}

- (IBAction)flipVerticalAction:(id)sender {
  if (self.shapePopoverDelegate &&
      [self.shapePopoverDelegate
          respondsToSelector:@selector(flipShapeVertical)]) {
    [self.shapePopoverDelegate flipShapeVertical];
  }
}

- (IBAction)deleteAction:(id)sender {
  if (self.shapePopoverDelegate &&
      [self.shapePopoverDelegate respondsToSelector:@selector(deleteShape)]) {
    [self.shapePopoverDelegate deleteShape];
  }
}
@end
