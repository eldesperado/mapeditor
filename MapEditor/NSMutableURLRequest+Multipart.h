//
//  NSMutableURLRequest+Multipart.h
//  Shoppin' Mate
//
//  Created by El Desperado on 7/10/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableURLRequest (Multipart)

+ (void)addMultipartDataWithParameters:(NSDictionary*)parameters
                          toURLRequest:(NSMutableURLRequest*)request;

@end
