//
//  StencilThumbnailViewController.h
//  MapEditor
//
//  Created by El Desperado on 7/17/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StencilThumbnailViewControllerDelegate;

@interface StencilThumbnailViewController : UICollectionViewController

@property(nonatomic, weak) id<StencilThumbnailViewControllerDelegate> delegate;
@property(strong, nonatomic) NSMutableArray* stencilThumbnails;

@end

@protocol StencilThumbnailViewControllerDelegate<NSObject>
@required
- (void)didSelectedStencilThumbnailImage:(UIImage*)image andId:(UInt16)stencilId;
@end