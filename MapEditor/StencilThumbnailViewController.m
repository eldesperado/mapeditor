//
//  StencilThumbnailViewController.m
//  MapEditor
//
//  Created by El Desperado on 7/17/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "StencilThumbnailViewController.h"

@interface StencilThumbnailViewController ()

@end

@implementation StencilThumbnailViewController

- (void)viewDidLoad {
  [super viewDidLoad];
}

- (NSInteger)collectionView:(UICollectionView*)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.stencilThumbnails count];
}

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView
                 cellForItemAtIndexPath:(NSIndexPath*)indexPath {
  static NSString* identifier = @"stencilThumbnailCell";

  UICollectionViewCell* cell =
      [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                forIndexPath:indexPath];

  UIImageView* recipeImageView = (UIImageView*)[cell viewWithTag:100];
  recipeImageView.image =
      [UIImage imageNamed:[self.stencilThumbnails objectAtIndex:indexPath.row]];

  return cell;
}

- (void)collectionView:(UICollectionView*)collectionView
    didSelectItemAtIndexPath:(NSIndexPath*)indexPath {
  if (self.delegate &&
      [self.delegate
          respondsToSelector:@selector(didSelectedStencilThumbnailImage:andId:)]) {
    NSString* imageName = [self.stencilThumbnails objectAtIndex:indexPath.row];
    UIImage* image = [UIImage imageNamed:imageName];
    if (image) {
      [self.delegate didSelectedStencilThumbnailImage:image andId:[imageName integerValue]];
      [self.navigationController popViewControllerAnimated:YES];
    }
  }
}
@end
