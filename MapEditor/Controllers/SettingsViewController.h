//
//  SettingsViewController.h
//  MapEditor
//
//  Created by Dat Truong on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsViewControllerDelegate <NSObject>

- (void)save;

@end

@interface SettingsViewController : UITableViewController

@property (nonatomic, weak) id<SettingsViewControllerDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *settings;

@end
