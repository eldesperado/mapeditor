//
//  PointSettingsViewController.h
//  MapEditor
//
//  Created by Dat Truong on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PointSettingsViewControllerDelegate <NSObject>

- (void)changedWithX:(CGFloat)x
                   y:(CGFloat)y
               minor:(UInt16)minor
               major:(UInt16)major;

- (void)deleteCurrentPoint;

@end

@interface PointSettingsViewController : UIViewController

@property (nonatomic, weak) id<PointSettingsViewControllerDelegate> delegate;

@property(strong, nonatomic) IBOutlet UITextField* xTextField;
@property(strong, nonatomic) IBOutlet UITextField* yTextField;
@property(strong, nonatomic) IBOutlet UITextField* minorTextField;
@property(strong, nonatomic) IBOutlet UITextField* majorTextField;

@end
