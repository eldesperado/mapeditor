//
//  PointSettingsViewController.m
//  MapEditor
//
//  Created by Dat Truong on 7/15/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "PointSettingsViewController.h"
#import "MEDBeaconPoint.h"

@interface PointSettingsViewController ()

@property (strong, nonatomic) MEDBeaconPoint *currentPoint;

@end

@implementation PointSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)done:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(changedWithX:y:minor:major:)]) {
        [self.delegate changedWithX:[self.xTextField.text floatValue] y:[self.yTextField.text floatValue] minor:[self.minorTextField.text integerValue] major:[self.majorTextField.text integerValue]];
    }
}

- (IBAction)deletePoint:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deleteCurrentPoint)]) {
        [self.delegate deleteCurrentPoint];
    }
}
@end
