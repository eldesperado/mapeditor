//
//  StencilCategoryViewController.m
//  MapEditor
//
//  Created by El Desperado on 7/17/14.
//  Copyright (c) 2014 Dat Truong. All rights reserved.
//

#import "StencilCategoryViewController.h"
#import "SPUserResizableView.h"
#import "StencilThumbnailViewController.h"

@interface StencilCategoryViewController ()<
    UITableViewDataSource,
    UITableViewDelegate,
    StencilThumbnailViewControllerDelegate> {
  NSArray* stencilCategoryNames;
  NSArray* stencilThumbnailLists;
}

@end

@implementation StencilCategoryViewController

#pragma mark - Initializtion
- (id)initWithCoder:(NSCoder*)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    stencilCategoryNames = @[ @"Shelf", @"Table", @"Cabinets", @"Desk" ];
    stencilThumbnailLists = @[
      @[
        @"1",
        @"2",
        @"3",
        @"4"
      ],
      @[
        @"5",
        @"6",
        @"7",
        @"8",
        @"9",
        @"10",
        @"11",
        @"12",
        @"13"
      ],
      @[ @"14", @"15" ],
      @[ @"16", @"17", @"18" ]
    ];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
}

#pragma mark - UITableViewDataSource

- (NSString*)tableView:(UITableView*)aTableView
    titleForHeaderInSection:(NSInteger)section {
  return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView*)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [stencilCategoryNames count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
  UITableViewCell* cell =
      [tableView dequeueReusableCellWithIdentifier:@"stencilCell"];

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:@"stencilCell"];
  }

  [self updateCell:cell atIndexPath:indexPath];

  return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView*)tableView
    didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
  NSArray* stencilThumbnailArray =
      [stencilThumbnailLists objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"showStencilThumbnails"
                            sender:stencilThumbnailArray];
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Private

- (void)updateCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath {
  cell.textLabel.text = @"";
  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  NSString* imageName = [stencilCategoryNames objectAtIndex:indexPath.row];
  if (indexPath.section == 0) {
    cell.textLabel.text = [imageName capitalizedString];
  }
}

- (void)setStencilWithImage:(UIImage*)image andId:(UInt16)stencilId{
  if (self.delegate &&
      [self.delegate respondsToSelector:@selector(didSelectedStencilWithImage:andId:)]) {
    if (image) {
      
      [self.delegate didSelectedStencilWithImage:image andId:stencilId];
    }
  }
}

#pragma mark - StencilThumbnailViewControllerDelegate
- (void)didSelectedStencilThumbnailImage:(UIImage*)image andId:(UInt16)stencilId{
  [self setStencilWithImage:image andId:stencilId];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"showStencilThumbnails"]) {
    if ([sender isKindOfClass:[NSArray class]]) {
      NSArray* thumbnails = (NSArray*)sender;
      StencilThumbnailViewController* stencilVC =
          [segue destinationViewController];
      stencilVC.delegate = self;
      stencilVC.preferredContentSize = CGSizeMake(320, 264);
      stencilVC.stencilThumbnails = [NSMutableArray arrayWithArray:thumbnails];
    }
  }
}

@end
